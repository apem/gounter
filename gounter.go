package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/apem/gounter/arrgs"
	"gitlab.com/apem/gounter/panner"
)

func main() {
	var words int64
	wordCount := make(panner.WordCounter)

	start := time.Now()
	hosts, err := ioutil.ReadDir(arrgs.Mirror)
	if err != nil {
		log.Fatalf("Error al leer %q: %s\n", arrgs.Mirror, err)
	}
	if len(hosts) == 0 {
		log.Fatalf("%q esta vacío.\n",
			strings.TrimPrefix(arrgs.Mirror, os.Getenv("HOME")))
	}

	go panner.HostGanger(hosts)
	panner.HostPannerPool()

	for range hosts {
		hostWordCT := <-panner.SummaryWordsCh
		for word, count := range hostWordCT {
			words += count
			wordCount[word] += count
		}
	}

	summary, err := os.Create(arrgs.Mirror + "summary_words")
	if err != nil {
		fmt.Printf("\n\n")
		fmt.Printf("Palabras %d\n", len(wordCount))
		fmt.Printf("Total palabras %10d\n", words)
		fmt.Printf("Tiempo total empleado %s\n",
			time.Since(start).Truncate(time.Millisecond))
		return
	}

	var total int64
	for word, count := range wordCount {
		total += count
		fmt.Fprintf(summary, "%-30s %10d\n", word, count)
	}
	fmt.Fprintf(summary, "\n%-30s %10d\n", "Total words", total)

	fmt.Printf("\n\n")
	fmt.Printf("Palabras %d\n", len(wordCount))
	fmt.Printf("Total palabras %10d\n", words)
	fmt.Printf("Tiempo total empleado %s\n",
		time.Since(start).Truncate(time.Millisecond))
}
