package panner

import (
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"

	"gitlab.com/apem/gounter/arrgs"
	"gitlab.com/apem/gounter/bouncer"
	"golang.org/x/net/html"
)

// Page estructura que representa la página a procesar.
type Page struct {
	dir  string
	file os.FileInfo
}

// WordCounter es el alias para una mapa de cadenas a enteros de 64 bits,
// para representar la frecuencia con que aparece una palabra.
type WordCounter map[string]int64

var (
	// processHostCh canal de comunicación que transporta estructuras que
	// representan las carpetas host a procesar.
	processHostCh chan os.FileInfo
	// SummaryWordsCh transporta los resúmenes de palabras por host, para
	// generar el resumen total de palabras de todos los host.
	SummaryWordsCh chan WordCounter
)

func init() {
	processHostCh = make(chan os.FileInfo)
	SummaryWordsCh = make(chan WordCounter)
}

// HostGanger realiza la función de distribuir la carga de trabajo de las
// carpetas host a procesar, al pasar el host a la tarea simultanea
func HostGanger(hosts []os.FileInfo) {
	for _, host := range hosts {
		processHostCh <- host
	}
}

// hostPanner es la tarea que se encarga de procesar los host, y a partir
// de la cual se genera el grupo de tareas simúltaneas.
func hostPanner(in <-chan os.FileInfo, out chan<- WordCounter) {
	for host := range in {
		wordCT := HostPanning(host)
		out <- wordCT
	}
}

// HostPannerPool función encargada de generar el grupo de tareas
// simúltaneas, que se distribuyen el procesamieno de los host.
func HostPannerPool() {
	hostPanners := arrgs.HostTask
	for hp := 1; hp <= hostPanners; hp++ {
		go hostPanner(processHostCh, SummaryWordsCh)
	}
}

// pageGanger realiza la función de distribuir la carga de trabajo de los
// archivos almacenados en un host,
func pageGanger(hostdir string, files []os.FileInfo, processFile chan Page) {
	for _, file := range files {
		processFile <- Page{hostdir, file}
	}
}

// pagePanner tarea encargada de procesar los archivos almacenados en un
// host, que sirve de base para crear el grupo individual de tareas
// simúltaneas para procesar los archivos por host.
func pagePanner(in <-chan Page, out chan<- WordCounter) {
	for elt := range in {
		wct, err := ProcessFile(elt.dir, elt.file)
		if err != nil {
			out <- nil
			continue
		}
		out <- wct
	}
}

// pagePannerPool función encargada de generar el grupo de tareas
// simúltaneas, que se distribuyen el procesamieno de los archivos que
// se encuentran en cada carpeta host.
func pagePannerPool(input <-chan Page, output chan<- WordCounter) {
	pagePanners := arrgs.PageTask
	for pp := 1; pp <= pagePanners; pp++ {
		go pagePanner(input, output)
	}
}

// HostPanning lee el host que se pasa como parametro y devuelve un contador
// de palabras que representa, el resumen de frecuencias de palabras que
// hay de los archivos que pertenecen al host.
func HostPanning(host os.FileInfo) (wordCount WordCounter) {
	var total int64
	hostpath := arrgs.Mirror + host.Name() + "/"

	processPageCh := make(chan Page)
	summaryPageCh := make(chan WordCounter)

	files, err := ioutil.ReadDir(hostpath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error al abrir host %q: %s\n", host.Name(), err)
		wordCount = nil
		return
	}
	if len(files) == 0 {
		fmt.Fprintf(os.Stderr, "Error %q esta vacío\n", host.Name())
		wordCount = nil
		return
	}

	wordCount = make(WordCounter)
	go pageGanger(hostpath, files, processPageCh)
	pagePannerPool(processPageCh, summaryPageCh)

	for range files {
		pageWordCount := <-summaryPageCh
		for word, count := range pageWordCount {
			total += count
			wordCount[word] += count
		}
	}
	hostfile, err := os.Create(hostpath + host.Name() + arrgs.Suffix)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error al crear archivo %q: %s\n", host.Name(), err)
		fmt.Printf("Palabras encontradas en %-30s %10d\n", host.Name(), total)
		return wordCount
	}

	for word, count := range wordCount {
		fmt.Fprintf(hostfile, "%-30s %10d\n", word, count)
	}
	fmt.Fprintf(hostfile, "\n%-30s %10d\n", "Total words", total)

	fmt.Printf("Palabras encontradas en %-30s %10d\n", host.Name(), total)
	return wordCount
}

// ProcessFile procesa el archivo actual contabilizando las palabras y
// guardando el resumen del conteo en un archivo. Devuelve una estructura
// WordCounter y error nulo, en caso de algún error al abrir el archivo
// se devuelve nulo y el error asociado.
func ProcessFile(hostpath string, file os.FileInfo) (wordCT WordCounter, err error) {
	var total int64
	filepath := hostpath + file.Name()
	page, err := os.Open(filepath)
	if err != nil {
		wordCT = nil
		err = fmt.Errorf("al abrir %q: %s", filepath, err)
		return
	}
	defer page.Close()

	wordCT = pagePanning(page)

	outfile, err := os.Create(page.Name() + arrgs.Suffix)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error al crear resumen para %s: %s\n", file.Name(), err)
		err = nil
		return
	}
	defer outfile.Close()

	for word, count := range wordCT {
		total += count
		fmt.Fprintf(outfile, "%-30s %10d\n", word, count)
	}
	fmt.Fprintf(outfile, "\n%-30s %10d\n", "Total words", total)
	err = nil
	return
}

// pagePanning procesa el archivo para cuantificar la frecuencia de las
// palabras encontradas, en las partes de texto del archivo. Devuelve una
// estructura WordCounter con las palabras encontradas y su frecuencia.
func pagePanning(page *os.File) (wct WordCounter) {
	wct = make(WordCounter)
	textCleaner := regexp.MustCompile("[^[:alpha:]áéíóúñÁÉÍÓÚÑ]")
	wordPanning := regexp.MustCompile("[[:alpha:]áéíóúñÁÉÍÓÚÑ]{2,}")

	tz := html.NewTokenizer(page)

	for {

		tt := tz.Next()

		switch {
		case tt == html.StartTagToken:
			tn := tz.Token().Data
			if tn == "a" || tn == "link" || tn == "script" || tn == "style" {
				continue
			}

			if tt = tz.Next(); tt != html.TextToken {
				continue
			}

			text := tz.Token().Data
			cleanText := textCleaner.ReplaceAllString(text, " ")
			wordsFound := wordPanning.FindAllString(cleanText, -1)

			for _, word := range wordsFound {
				wordToCount := strings.ToLower(word)
				if inStopWords := bouncer.Dict[wordToCount]; inStopWords {
					continue
				}
				wct[wordToCount]++
			}
		case tt == html.ErrorToken:
			return
		default:
			continue
		}
	}
}
